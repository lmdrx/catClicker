# App Structure

## 0. Helpers
	+ _id

## A. Model
	+ Properties
		* last
		* data
	+ Methods
		* create
		* get
		* change
		* destroy
		* all

## B. Octopus
	+ Methods
		* add
		* remove
		* update (TODO)
		* vote
		* api
		* init

## C. UI
### C.1 - List
	+ Properties
		* target
		* elements
	+ Methods
		* render
		* createInstance
		* addClickEvent
		* hide
### C.2 - Viewer
	+ Properties
		* DOMtarget
		* voteCounter
		* displayName
		* currentItem
	+ Methods
		* display
		* listenForClicks
### C.3 - Admin
	+ Properties
	+ Methods