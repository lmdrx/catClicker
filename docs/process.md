# Process Notes
# --TODO: Rewrite--#

## 1. Start with a simple structure.
Write a simple scheme of the necessary parts of the app in a sheet of paper or a markdown document. Then take the primary and minimum concepts and organize it into a MVC scheme (Model, View, Controller). The controller module should be in the middle of the other two as a proxy and mediator, working as the app's main hub.

## 2. Build the basic static UI.
This is going to allow to have a better idea of how everything will be bound together, also will provide a bit of motivation as it allows to visualize how the project will be perceived by the end user.

## 3. (Encapsulation)
Everything will be encapsulated inside an IIFE to provide context privacy to the modules. Only selected functionalities will be bound to the global api object (which will be named 'Paws').