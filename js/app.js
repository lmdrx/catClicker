(function() {
  'use strict';

  /**
  Helper
  -------------------------------------------------------------------------*/
  var _id = function(id){ return document.getElementById(id); };

  /**
  Model
  -------------------------------------------------------------------------*/
  var model = {
    // Properties
    last: 0,
    data: [],
    // Methods
    // [model.create] Create a new item and store it in the data array
    create : function(name, image, imageRef) {
      if(name && typeof name === 'string') {
        model.data.push({
          id :  ++model.last,
          name : name,
          img : image,
          imgRef : imageRef,
          votes : 0,
          visible : true
        });
        return model.get(model.last);
      }
    },
    // [model.get] Get a particular item from the data array
    get : function(idInt) {
      return model.data[idInt - 1];
    },
    // [model.change] Change properties and info on existing item
    change : function(idInt, newParamsObj) {
      return (function(obj) {
        if(typeof idInt === 'number') {
          for(var i = 0; i < arguments.length; i++) {
            for (var prop in arguments[i]) {
              var value = arguments[i][prop];
              if (typeof value == "object") {
                model.change(obj[prop], value);
              } else {
                if(value){
                  obj[prop] = value;
                } else {
                  void 0;
                }
              }
            }
          }
          return obj;
        }
      }( model.get(idInt), newParamsObj) );
      // Inspired on: http://stackoverflow.com/a/12534361/2260466
    },
    // [model.destroy] Deletes a item (Just kiddin', only hides it...)
    destroy : function(model) {
      if( model ) {
        model.visible = false;
        return model;
      }
    },
    // [model.all] Get all 'visible' items from the data array.
    all: function() {
      var arr = [];
      model.data.forEach(function(item) {
        return arr.push(item);
      });
      return arr;
    }
  };

  /**
  Logic
  -------------------------------------------------------------------------*/
  var logic = {
    // [logic.add] Add a new item - creates model and renders it.
    add: function(name, image, imageRef) {
      // TODO: Qué pasa si no le pasas un segundo argumento?
      // 		Tal vez sea mejor recibir un objecto como model.update()
      if(arguments[0].length > 1 && typeof arguments[0] === 'string') {
        var itemObj = model.create(name, image, imageRef);
        UI.list.render(itemObj);
        UI.viewer.display(itemObj);
        return itemObj;
      } else {
        arguments[0].forEach(function(a) {
          logic.add(a);
        });
      }
    },
    // [logic.remove] Removes an item
    remove: function(id){
      var instance = model.get(id);
      UI.list.hide(instance);
      model.destroy(instance);
    },
    // [logic.update]
    update: function(){
      var current = UI.viewer.current
      model.change(current.id, {
        name: _id( 'nameInput' ).value,
        img: _id( 'urlInput' ).value,
        visible: _id( 'visibilityToggle' ).checked,
      });
      UI.list.bindInnerText(UI.list.elements[current.id - 1], current.name);
      UI.viewer.display(current);
    },
    // [logic.vote] takes an object and grow its vote count by one
    vote: function(obj) {
      UI.viewer.voteCounter.innerHTML = ++obj.votes;
    },
    // [logic.api] Public access to model data through API-like-Object
    api : function() {
      // App Holder:
      APP.add = this.add;
      APP.remove = this.remove;
    },
    // [logic.init] Initializes the mighty logic
    init : function() {
      logic.api();
      UI.viewer.listenForClicks();
      UI.admin.init();
    }
  };

  /**
  Views
  -------------------------------------------------------------------------*/
  var UI = {
    list: {
      // Properties
      target: _id( 'list' ),
      elements: [],
      // Methods
      // [list.render] Renders a new DOM instance with provided object.
      render: function(obj) {
        if(obj.visible) {
          var instance = this.createInstance(obj);
          this.addClickEvent(obj, instance);
        }
      },
      bindInnerText: function(instance, name){
        instance.innerHTML = '<button class="button">'+name+'</button>';
      },
      // [list.createInstance]
      createInstance: function(obj){
        var instance = document.createElement('li');
        this.bindInnerText(instance, obj.name);
        this.target.appendChild(instance);
        this.elements.push(instance);
        return instance;
      },
      // [list.addClickEvent]
      addClickEvent: function(obj, inst){
        inst.addEventListener('click', (function() {
          return function() {
            UI.viewer.display(obj);
          };
        }()));
      },
      // [list.remove] remove a rendered item from the DOM
      hide: function(obj) {
        this.target.removeChild(this.elements[obj.id - 1])
      }
    },
    viewer: {
      // Properties
      DOMtarget: 	 _id( 'displayImage' ),
      voteCounter: _id( 'voteCounter' ),
      displayName: _id( 'displayName' ),
      displayImgRef: _id( 'displayImgRef' ),
      current: undefined,

      // Methods
      // [viewer.display]
      display: function(obj) {
        this.current = obj;
        this.displayName.innerHTML = obj.name;
        this.voteCounter.innerHTML = obj.votes;
        this.displayImgRef.innerHTML ='<span>Source: </span><a href="'+
        obj.imgRef+'">'+obj.imgRef+'</a>';
        this.DOMtarget.src = obj.img;
      },
      // [view.listenForClicks]
      listenForClicks: function(){
        this.DOMtarget.addEventListener('click',
        (function(instance) { return function() {
          logic.vote(instance.current);
        };
      }(this)));
    }
  },
  admin: {
    // Properties
    target: 			_id( 'admin' ),
    adminButton: 		_id( 'adminTrigger' ),

    form: document.createElement('div'),
    template: '<label for="nameInput"> Name' +
    '<input id="nameInput" type="text" placeholder="Name">' +
    '</label></br>' +
    '<label for="urlInput"> URL' +
    '<input id="urlInput" type="text" placeholder="URL">' +
    '</label></br>' +
    '<label for="visibilityToggle"> Visible' +
    '<input id="visibilityToggle" type="checkbox">' +
    '</label></br>' +
    '<button id="cancelButton" class="button">Cancel</button>'+
    '<button id="saveButton" class="button">Save</button>',

    // Methods
    // [UI.admin.adminMode]
    toggle: function(){
      var section = UI.admin.target;
      var form = UI.admin.form;
      if(!section.childNodes.length){
        form.innerHTML = UI.admin.template;
        section.appendChild(form);
        _id( 'saveButton' ).addEventListener('click', (function(){
          return function(){
            logic.update();
            section.removeChild(section.firstChild);
          };
        }(this)));
        _id( 'cancelButton' ).addEventListener('click',(function(){
          return function(){
            return section.removeChild(section.firstChild);
          };
        }(this)));
      } else {
        section.removeChild(section.firstChild);
      };
    },
    init: function(){
      this.adminButton.addEventListener('click', (function(){
        return function(){
          UI.admin.toggle();
        };
      }(this)));
    }
  }
};
logic.init();
}( window.APP = window.APP || {} ));
