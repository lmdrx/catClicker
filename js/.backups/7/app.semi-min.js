var Paws = Paws || {};
! function() {
    "use strict";
    var a = {
            current: 0,
            data: [],
            create: function(b, c, d) {
                return b && "string" == typeof b && a.data.push({
                    id: ++a.current,
                    name: b || "Anonymous Cat",
                    img: c || "img/placeholder.png",
                    imgThumb: c,
                    imgRef: d || "Uncredited Image",
                    votes: 0,
                    visible: !0
                }), a.data[a.current - 1]
            },
            read: function(b) {
                return a.data[b - 1]
            },
            update: function(b, c) {
                "number" == typeof b && ! function(a) {
                    for (var b = 0; b < arguments.length; b++)
                        for (var c in arguments[b]) {
                            var d = arguments[b][c];
                            "object" == typeof d ? modify(a[c], d) : a[c] = d
                        }
                    return a
                }(a.read(b), c)
            },
            destroy: function(b) {
                return a.read(b) ? (a.read(b).visible = !1, a.read(b)) : void 0
            },
            index: function() {
                var b = [];
                return a.data.forEach(function(a) {
                    return b.push(a)
                }), b
            }
        },
        b = {
            render: function(a) {
                arguments[0].length > 1 ? arguments[0].forEach(function(a) {
                    b.render(a)
                }) : a.DOMinstance = c.list.add(a)
            },
            vote: function(a) {
                return ++a.votes
            },
            api: function() {
                console.log(Paws["Kitty, Kitty"] = "Purrrrrrr.... ")
            },
            init: function() {
                b.api(), d.model(), b.render(a.index())
            }
        },
        c = {
            list: {
                target: document.getElementById("list"),
                elements: [],
                add: function(a) {
                    if (a.visible) {
                        var b = document.createElement("li");
                        return b.innerHTML = '<a href="#"><span>' + a.name + "</span></a>", b.addEventListener("click", function(b) {
                            return function() {
                                c.display.update(a)
                            }
                        }()), this.target.appendChild(b), b
                    }
                },
                remove: function(a) {
                    c.list.target.removeChild(a.DOMinstance)
                }
            },
            display: {
                target: document.getElementById("displayImage"),
                update: function(a) {
                    this.target.src = a.img
                }
            }
        },
        d = {
            model: function() {
                console.groupCollapsed("Model CRUD Tests"), console.log("Create (1) \n", a.create("Espanto", "img/Espanto.png")), console.log("Create (2) \n", a.create("Cucho", "img/Cucho.png")), console.log("Create (3) \n", a.create("Benito", "img/Benito.png")), console.log("Create (4) \n", a.create("Dem\xf3stenes", "img/Dem\xf3stenes.png")), console.log("Create (5) \n", a.create("Panza", "img/Panza.png")), console.log("Create (6) \n", a.create("Matute", "img/Matute.png")), console.log("Read (4) \n", a.read(4)), console.log("Destroy (6) \n", a.destroy(6)), console.log("Index \n", a.index()), console.groupEnd()
            }
        };
    b.init()
}();
