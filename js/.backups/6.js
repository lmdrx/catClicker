var Paws = Paws || {};

;(function() {
	var model = {
		current: 0,
		data: [],
		// Create a new cat and store it in the data array
		create : function(nameStr, imgStr, imgRefStr){
			if(nameStr && typeof nameStr === 'string'){
				model.data.push({
					id :  ++model.current,
					name : nameStr || 'Anonymous Cat',
					img : imgStr || 'img/placeholder.png',
					imgThumb : imgStr,
					imgRef : imgRefStr || 'Uncredited Image',
					votes : 0,
					visible : true
				});
			};
			return model.data[model.current-1];
		},
		// Get a particular cat from the data array
		read : function(idInt){
			return model.data[idInt-1];
		},
		// Update info on existing cat
		update : function(idInt, editedObj){
			if(typeof idInt === 'number'){
				(function(obj) {
					for(var i = 0; i < arguments.length; i++) {
						for (var prop in arguments[i]) {
							var value = arguments[i][prop];
							if (typeof value == "object") {
								modify(obj[prop], value);
							} else {
								obj[prop] = value;
							}
						}
					}
					return obj;
				}(model.read(idInt), editedObj));
				// Inspired on: http://stackoverflow.com/a/12534361/2260466
			}
		},
		// Deletes a cat (Just kiddin', only makes it non-visible)
		destroy : function(idInt){
			if(model.read(idInt)){
				model.read(idInt).visible = false;
				return model.read(idInt);
			}
		},
		// Get all 'visible' cats from the data array.
		index: function(){
			// Original returns an array of visible elements.
			var arr = [];
			model.data.forEach(function(cat){
				return arr.push(cat);
			});
			return arr;
		}
	};

	var octopus = {
		// [render] Set views with the model data.
		// This works a single object... but the function is ready to
		// take model.index (which returns an array with objects by breaking it
		// apart and iterating on it).
		// When we have a single object as argument[0] then we send it
		// to the view's "add to list" method, after we set the visible
		// parameter to 'true' since it wont take it if it's not visible.
		render: function(cat){
			if(arguments[0].length > 1 ) {
				arguments[0].forEach(function(a){
					octopus.render(a);
			});
				} else {
					cat.DOMinstance = view.list.add(cat);
			}
		},
		// octopus[vote] takes an object and grow its vote count by one
		vote : function(cat){
			return ++cat.votes;
		},
		// octopus[api]: Public access to the model data through a API-like-Object
		api : function(){
			// for the LULZ
			console.log(Paws['Kitty, Kitty'] = 'Purrrrrrr.... ');
			// PUBLIC ACCESS METHODS
			Paws.create 	= model.create;
			Paws.get 	= model.read;
			Paws.update 	= model.update;
			Paws.remove 	= model.destroy;
			Paws.index 	= model.index;
		},
		// octopus[init] Initializes the mighty octopus
		init : function(){
			octopus.api();
			tests.model();
			octopus.render(model.index());
		}
	};
	// [view]
	var view = {
		// view[list]
		list: {
			target: document.getElementById( 'list' ),
			elements: [],
			// [add] Adds a new DOM instance with the provided object.
			add: function(obj){
				if(obj.visible){
					// This is the only time a view talks to a model (obj.DOMinstance)
					var inst = document.createElement('li');
					inst.innerHTML = '<a href="#"><span>'+obj.name+'</span></a>';
					inst.addEventListener('click', (function(catInstance) {
						return function(){
							view.display.update(obj);
							// console.log('click');
						};
					})());
					this.target.appendChild(inst);
					return inst;
				}
			},
			remove: function(obj){
				view.list.target.removeChild(obj.DOMinstance);
			}
		},
		// [display]
		display: {
			target: document.getElementById('displayImage'),
			update: function(cat){
				this.target.src = cat.img;
				// this.target.src = cat.img;
				// console.log('display update: ' + );
				// <img id="displayImage" src="img/placeholder.png" width="400" alt="name">
			}
		}
	};
	// [tests] Raw Model Tests
	var tests = {
		model: function(){
			console.groupCollapsed('Model CRUD Tests');
			console.log( 'Create (1) \n', model.create('Espanto', 'img/Espanto.png') );
			console.log( 'Create (2) \n', model.create('Cucho', 'img/Cucho.png') );
			console.log( 'Create (3) \n', model.create('Benito', 'img/Benito.png') );
			console.log( 'Create (4) \n', model.create('Demóstenes', 'img/Demóstenes.png') );
			console.log( 'Create (5) \n', model.create('Panza', 'img/Panza.png') );
			console.log( 'Create (6) \n', model.create('Matute', 'img/Matute.png') );
			console.log( 'Read (4) \n', model.read(4) );
			console.log( 'Destroy (6) \n', model.destroy(6) );
			console.log( 'Index \n', model.index() );
			console.groupEnd();
		}
	}

	octopus.init();
}());